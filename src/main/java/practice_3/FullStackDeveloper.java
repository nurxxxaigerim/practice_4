package practice_3;

public class FullStackDeveloper extends Worker implements BackendDeveloper, FrontendDeveloper {

    public FullStackDeveloper(String name) {
        super(name);
    }

    @Override
    public void writeBack() {
        System.out.println("My name is " + getName() + ", I'm writing back in C++!");
    }

    @Override
    public void writeFront() {
        System.out.println("My name is " + getName() + ", I'm writing front in CSS!");
    }

    @Override
    public void develop() {
        writeFront();
        writeBack();
    }

    @Override
    public void work() {
        develop();
    }
}
