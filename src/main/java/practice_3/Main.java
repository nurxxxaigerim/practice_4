package practice_3;

public class Main {
    public static void main(String[] args){

        Company company = new Company();
        company.addEmployee(new JavaDeveloper("Aigerim"));
        company.addEmployee(new JavascriptDeveloper("Gaukhar"));
        company.addEmployee(new FullStackDeveloper("Yelina"));
        company.startWork();
    }
}

