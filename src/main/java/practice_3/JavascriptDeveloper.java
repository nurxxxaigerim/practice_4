package practice_3;

public class JavascriptDeveloper extends Worker implements FrontendDeveloper, TeaMaker{
    public JavascriptDeveloper(String name) {
        super(name);
    }

    @Override
    public void develop() {
        writeFront();
    }

    @Override
    public void work() {
        develop();
        makeTea();
    }

    @Override
    public void writeFront() {
        System.out.println("My name is " + getName() + ", I'm writing front in Javascript!");
    }

    @Override
    public void makeTea() {
        System.out.println("Here's your tea!");
    }
}
