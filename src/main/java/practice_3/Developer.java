package practice_3;

public interface Developer extends Person{
    void develop();
}
