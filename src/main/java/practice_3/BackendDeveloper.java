package practice_3;

public interface BackendDeveloper extends Developer {
    void writeBack();
}
